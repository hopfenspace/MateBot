"""
MateBot core
"""

__all__ = ["api", "misc", "persistence", "schemas", "settings"]
__version__ = "0.4.2"
